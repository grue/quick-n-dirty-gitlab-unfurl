import os
import re
import gitlab
import slack_sdk
import logging

from slack_bolt import App
from slack_bolt.adapter.aws_lambda import SlackRequestHandler

app = App(
    token=os.environ.get("SLACK_BOT_TOKEN"),
    signing_secret=os.environ.get("SLACK_SIGNING_SECRET"),
    process_before_response=True
)

gitlab_domain = os.environ.get("GITLAB_DOMAIN", "gitlab.com")
gitlab_url = f"https://{gitlab_domain}"
gitlab_emoji = os.environ.get("SLACK_GITLAB_EMOJI",":gitlab:")
gitlab_unknown_avatar_image_url = os.environ.get("UNKNOWN_AVATAR_URL", "https://cdna.pcpartpicker.com/static/forever/images/user/2215853.add2d9f88f1779cc1ff8a3caeaa3b714.256c.jpg")

gl = gitlab.Gitlab(url=gitlab_url,
                   private_token=os.environ.get("GITLAB_ACCESS_TOKEN"))


def parse_gitlab_link_type(url: str) -> tuple:
    # tested against:
    # https://gitlab.com/splatops/cntn-signalbackup-tools
    # https://gitlab.com/splatops/cntn-signalbackup-tools/-/merge_requests/1
    # https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning/-/blob/master/CHANGELOG.md
    # https://gitlab.com/splatops/cntn-signalbackup-tools/-/commit/80f35754037cf4b92786ce6ebaabce1e6ba03c3f
    exp = re.compile(
        f"{gitlab_domain}/(?P<Namespace>[\w\d\-_]*)/(?P<Project>[\w\d\-_/]*)(/-/(?P<LinkType>blob|merge_requests|.*)/(?P<ResourceID>.*)/?.*|[^/\-\d\w]$)")
    results = exp.findall(url)
    return results[0] if len(results) == 1 else ("unknown", "unknown", "unknown", "unknown", "unknown")

@app.event(event="link_shared")
def link_shared(client: slack_sdk.web.client.WebClient, event: dict):
    unfurls = {}
    
    for l in event.get("links", []):
        unfurls[l["url"]] = render_unfurls_for_link(l["url"])

    client.chat_unfurl(
        source=event["source"], unfurl_id=event["unfurl_id"], unfurls=unfurls)


def fetch_commit_details(url:str):
    namespace, project, _, link_type, resource_id = parse_gitlab_link_type(url)
    logging.info("commit", namespace, project, link_type, resource_id)
    p = gl.projects.get(id=f"{namespace}/{project}", lazy=True)
    c = p.commits.get(resource_id)
    return c.attributes.get("title"), c.attributes.get("message"), c.attributes.get("author_email"), c.attributes.get("stats", {})


def fetch_project_details(url:str):
    namespace, project, _, link_type, resource_id = parse_gitlab_link_type(url)
    p = gl.projects.get(id=f"{namespace}/{project}")
    return p

def fetch_merge_request_details(url:str):
    namespace, project, _, link_type, resource_id_raw = parse_gitlab_link_type(url)
    resource_id = resource_id_raw.split('#')[0].split('/')[0]
    logging.info("mr", namespace, project, link_type, resource_id, resource_id_raw)
    p = gl.projects.get(id=f"{namespace}/{project}", lazy=True)
    return p.mergerequests.get(id=resource_id)


def render_unfurls_for_link(url:str):
    namespace, project, _, link_type, resource_id = parse_gitlab_link_type(url)
    project_details = fetch_project_details(url)
    
    project_description = project_details.attributes["description"]
    project_emoji = gitlab_emoji

    # If you want to make custom emojis per project, i suppose here?
    # if project_details.attributes['path'].endswith("sentinel"):
    #     project_emoji = ":sentinel:"
    # elif project_details.attributes['path'].endswith("something-cool"):
    #     project_emoji = ":something-cool:"
    # elif project_details.attributes['path'].endswith("gitlab"):
    #     project_emoji = ":party-gitlab:"

    blocks = []
    contexts = [
        {
            "type": "context",
            "elements": [
                {
                    "type": "mrkdwn",
                    "text": f"*Project*: <{project_details.attributes['web_url']}|{project_details.attributes['name']}>"
                }
            ]
        }
    ]

    if link_type == "commit":
        title, message, author_email, stats = fetch_commit_details(url)
        commit_short = resource_id[8:]

        blocks.extend([
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f"{project_emoji} Gitlab - Commit"
                },
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f"<{url}|{title} ({commit_short}) · Commits · {namespace} / {project} · GitLab>"
                }
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f"{message}"
                }
            }
        ])

        contexts.extend([
            {
                "type": "context",
                "elements": [
                    {
                        "type": "mrkdwn",
                        "text": f"*Author*:"
                    },
                    {
                        "type": "mrkdwn",
                        "text": f"{author_email}"
                    }
                ]
            },
            {
                "type": "context",
                "elements": [
                    {
                        "type": "mrkdwn",
                        "text": f"*Stats*:"
                    },
                    {
                        "type": "mrkdwn",
                        "text": f"*additions*: +{stats.get('additions')}"
                    },
                    {
                        "type": "mrkdwn",
                        "text": f"*deletions*: -{stats.get('deletions')}"
                    },
                    {
                        "type": "mrkdwn",
                        "text": f"*total changes*: ±{stats.get('total')}"
                    },
                ]
            }
        ])
    elif link_type == "blob":
        file_name = resource_id.split('/')[-1].split('#')[0]
        branch = resource_id.split('/')[0]
        blocks.extend([
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f"{project_emoji} Gitlab - File"
                },
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f"<{url}|{file_name} · {branch} · {gitlab_domain} / {namespace} / {project} · GitLab>"
                }
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f"{project_description}"
                }
            }
        ])
    elif link_type == "merge_requests":
        mr = fetch_merge_request_details(url)
        title = str(mr.attributes.get("title", "error"))
        author = str(mr.attributes.get("author", {}).get("name", "error"))
        author_email = str(mr.attributes.get("author", {}).get("email", "error"))
        avatar_url = str(mr.attributes.get("author", {}).get("avatar_url", gitlab_unknown_avatar_image_url))
        assignee = mr.attributes.get("assignee", {})
        assignees = mr.attributes.get("assignees", [])

        list(assignees).append(assignee)
        full_assignees = ", ".join(set([ person["name"] for person in filter(lambda x: x != None and "name" in x, assignees)])) or "None"
        
        state = str(mr.attributes.get("state", "error"))
        iid = str(mr.attributes.get('iid'))
        
        blocks.extend([
            {
                "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": f"{project_emoji} Gitlab - Merge Request"
                        },
            },
            {
                "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": f"*<{url}|{title} (!{iid}) · {author} / {project}>*"
                        }
            }
        ])
        contexts.extend([
            {
                "type": "context",
                "elements": [
                    {
                        "type": "mrkdwn",
                        "text": f"*Status*"
                    },
                    {
                        "type": "plain_text",
                        "text": f"{state}",
                        "emoji": True
                    }
                ]
            },
            {
                "type": "context",
                "elements": [
                    {
                        "type": "mrkdwn",
                        "text": f"*Author*:"
                    },
                    {
                        "type": "image",
                        "image_url": avatar_url,
                        "alt_text": "author avatar"
                    },
                    {
                        "type": "mrkdwn",
                        "text": f"{author}"
                    }
                ]
            },
            {
                "type": "context",
                "elements": [
                    {
                        "type": "mrkdwn",
                        "text": f"*Assignee(s)*"
                    },
                    {
                        "type": "plain_text",
                        "text": f"{full_assignees}",
                        "emoji": True
                    }
                ]
            }
        ])
    else:
        blocks.extend([
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f"{project_emoji} Gitlab"
                }
            }
        ])

    blocks.extend(contexts)

    preview = {
        "title": {
            "type": "plain_text",
            "text": f"{project_emoji} Gitlab - Project: {project}"
        }
    }

    return {
        "blocks": blocks,
        "preview": preview
    }


# Start your app
if __name__ == "__main__":
    app.start(port=int(os.environ.get("PORT", 3000)))
    
SlackRequestHandler.clear_all_log_handlers()
logging.basicConfig(format="%(asctime)s %(message)s", level=logging.DEBUG)


def lambda_handler(event, context):
    slack_handler = SlackRequestHandler(app=app)
    return slack_handler.handle(event, context)
